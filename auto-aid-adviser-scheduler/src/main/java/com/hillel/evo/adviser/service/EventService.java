package com.hillel.evo.adviser.service;

import com.hillel.evo.adviser.dto.EventBusinessDto;
import com.hillel.evo.adviser.dto.EventCreateDto;
import com.hillel.evo.adviser.dto.EventShortDto;
import com.hillel.evo.adviser.dto.EventUserDto;

import java.time.YearMonth;
import java.util.List;

public interface EventService {

    EventUserDto createEvent(Long businessId, EventCreateDto createDto);

    void confirmEvent(Long eventId);

    void completeEvent(Long eventId);

    void declineEvent(Long eventId);

    List<EventShortDto> getMonthlyEventsShort(YearMonth yearMonth, Long businessId);

    List<EventBusinessDto> getMonthlyBusinessEvents(YearMonth yearMonth, Long businessId);

    List<EventUserDto> getMonthlyUserEvents(YearMonth yearMonth, Long userId);

    void deleteBusinessEvents(Long businessId);
}
