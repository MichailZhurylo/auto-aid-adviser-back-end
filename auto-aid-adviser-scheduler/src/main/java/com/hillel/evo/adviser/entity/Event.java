package com.hillel.evo.adviser.entity;

import com.hillel.evo.adviser.enums.EventStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
@NamedEntityGraph(
        name = "Event.user",
        attributeNodes = {
                @NamedAttributeNode(value = "id"),
                @NamedAttributeNode("business"),
                @NamedAttributeNode("service"),
                @NamedAttributeNode("date"),
                @NamedAttributeNode("startTime"),
                @NamedAttributeNode("endTime"),
                @NamedAttributeNode("status")
        }
)
@NamedEntityGraph(
        name = "Event.business",
        attributeNodes = {
                @NamedAttributeNode("id"),
                @NamedAttributeNode(value = "car", subgraph = "car"),
                @NamedAttributeNode("service"),
                @NamedAttributeNode("date"),
                @NamedAttributeNode("startTime"),
                @NamedAttributeNode("endTime"),
                @NamedAttributeNode("status")
        },
        subgraphs = {
                @NamedSubgraph(name = "car", attributeNodes = {
                        @NamedAttributeNode("simpleUser"),
                        @NamedAttributeNode(value = "carModel", subgraph = "car.model"),
                        @NamedAttributeNode("images")}),
                @NamedSubgraph(name = "car.model", attributeNodes = {
                        @NamedAttributeNode("carBrand"),
                        @NamedAttributeNode("typeCar")})
        }
)
@NamedEntityGraph(
        name = "Event.short",
        attributeNodes = {
                @NamedAttributeNode("id"),
                @NamedAttributeNode("date"),
                @NamedAttributeNode("startTime"),
                @NamedAttributeNode("endTime")
        }
)
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private UserCar car;

    @ManyToOne(fetch = FetchType.LAZY)
    private Business business;

    @ManyToOne(fetch = FetchType.LAZY)
    private ServiceForBusiness service;

    private LocalDate date;

    private LocalTime startTime;

    private LocalTime endTime;

    @Enumerated(EnumType.STRING)
    private EventStatus status;
}
