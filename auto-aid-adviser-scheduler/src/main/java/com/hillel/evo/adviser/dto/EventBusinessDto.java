package com.hillel.evo.adviser.dto;

import com.hillel.evo.adviser.enums.EventStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@NoArgsConstructor
public class EventBusinessDto {

    private Long id;

    private UserCarEventDto car;

    private ServiceForBusinessShortDto service;

    private LocalDate date;

    private LocalTime startTime;

    private LocalTime endTime;

    private EventStatus status;
}
