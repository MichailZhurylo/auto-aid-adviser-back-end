package com.hillel.evo.adviser.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@NoArgsConstructor
public class EventCreateDto {

    private Long carId;

    private Long serviceId;

    private LocalDate date;

    private LocalTime startTime;
}
