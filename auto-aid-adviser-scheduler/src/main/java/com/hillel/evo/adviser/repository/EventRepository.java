package com.hillel.evo.adviser.repository;

import com.hillel.evo.adviser.entity.Event;
import com.hillel.evo.adviser.enums.EventStatus;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

    @EntityGraph(value = "Event.short")
    @Query("select e from Event e where e.business.id = :id and e.status <> :status " +
            "and month(e.date) = :month and year(e.date) = :year")
    List<Event> findEventsByMonth(@Param("month") Integer month, @Param("year") Integer year,
                                  @Param("id") Long businessId, @Param("status") EventStatus status);

    @EntityGraph(value = "Event.business")
    @Query("select e from Event e where e.business.id = :id and month(e.date) = :month and year(e.date) = :year ")
    List<Event> findEventsByMonthAndBusiness(@Param("month") Integer month, @Param("year") Integer year,
                                             @Param("id") Long businessId);

    @EntityGraph(value = "Event.user")
    @Query("select e from Event e where e.car.simpleUser.id = :id and month(e.date) = :month and year(e.date) = :year ")
    List<Event> findEventsByMonthAndUser(@Param("month") Integer month, @Param("year") Integer year,
                                             @Param("id") Long userId);

    @Query("select e from Event e where e.business.id = :id")
    List<Event> findAllByBusiness(@Param("id") Long businessId);

    @Query("select case when count(e) > 0 then true else false end " +
            "from Event e where e.business.id = :id and e.date = :date and " +
            "(e.startTime between :start and :end or e.endTime between :start and :end)")
    boolean hasEvent(@Param("id") Long businessId, @Param("start") LocalTime startTime,
                     @Param("end") LocalTime endTime, @Param("date") LocalDate date);

    @Query("select max(e) from Event e where e.business.id =:id and " +
            "e.date = :date and e.startTime < :start order by e.startTime desc")
    Event findNearestEvent(@Param("id") Long businessId, @Param("start") LocalTime startTime,
                           @Param("date") LocalDate date);

    @Modifying
    @Query("delete from Event e where e.business.id = :id")
    void deleteAllByBusinessId(@Param("id") Long businessId);
}
