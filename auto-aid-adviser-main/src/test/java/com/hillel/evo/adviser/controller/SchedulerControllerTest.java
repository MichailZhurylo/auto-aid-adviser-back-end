package com.hillel.evo.adviser.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hillel.evo.adviser.AdviserStarter;
import com.hillel.evo.adviser.dto.EventCreateDto;
import com.hillel.evo.adviser.entity.AdviserUserDetails;
import com.hillel.evo.adviser.enums.EventStatus;
import com.hillel.evo.adviser.repository.AdviserUserDetailRepository;
import com.hillel.evo.adviser.service.JwtService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.YearMonth;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {AdviserStarter.class, TestDatabaseService.class})
@AutoConfigureMockMvc
@Sql(value = "/data-event-test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class SchedulerControllerTest {

    @Autowired
    private TestDatabaseService dbService;

    @Autowired
    private AdviserUserDetailRepository userRepository;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    JwtService jwtService;

    @Autowired
    ObjectMapper objectMapper;

    private static final String PATH_SCHEDULER = "/scheduler";
    private static final String USER_EMAIL = "svg@mail.com";
    private static final String BUSINESS_EMAIL = "bvg@mail.com";

    private static String jwtUser;
    private static String jwtBusiness;
    private static EventCreateDto createDto;
    private static Long businessId;
    private static Long eventId;
    private static Long userId;
    private static YearMonth yearMonth;

    @BeforeEach
    void setUp(){
        AdviserUserDetails user = userRepository.findByEmail(USER_EMAIL).get();
        jwtUser = jwtService.generateAccessToken(user.getId());
        AdviserUserDetails businessUser = userRepository.findByEmail(BUSINESS_EMAIL).get();
        jwtBusiness = jwtService.generateAccessToken(businessUser.getId());
        Long serviceId = dbService.getId("service", "name", "straightening dents");
        Long carId = dbService.getId("user_car", "release_year", "2016");
        businessId = dbService.getId("business", "name", "user 1 STO 1");
        eventId = dbService.getId("event", "car_id", String.valueOf(carId));
        userId = dbService.getId("adviser_usr", "email", "svg@mail.com");
        yearMonth = YearMonth.of(2019,12);
        createDto = new EventCreateDto();
        createDto.setCarId(carId);
        createDto.setDate(LocalDate.now());
        createDto.setServiceId(serviceId);
        createDto.setStartTime(LocalTime.now());
    }

    @AfterEach
    void truncate() {
        dbService.truncate();
    }

    @Test
    void whenGetMonthlySchedulerByBusinessIdShouldReturnThem() throws Exception {
        mockMvc.perform(get(PATH_SCHEDULER + "/{business-id}/{year-month}", businessId, yearMonth)
                .header("Authorization", JwtService.TOKEN_PREFIX + jwtUser))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    void whenGetMonthlySchedulerForBusinessThenReturnThem() throws Exception {
        mockMvc.perform(get(PATH_SCHEDULER + "/business/{business-id}/{year-month}", businessId, yearMonth)
                .header("Authorization", JwtService.TOKEN_PREFIX + jwtBusiness))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    void whenGetMonthlySchedulerForUserThenReturnThem() throws Exception {
        mockMvc.perform(get(PATH_SCHEDULER + "/user/{user-id}/{year-month}", userId, yearMonth)
                .header("Authorization", JwtService.TOKEN_PREFIX + jwtUser))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    void whenCreateEventShouldCreateIt() throws Exception {
        mockMvc.perform(post(PATH_SCHEDULER + "/{business-id}", businessId)
                .header("Authorization", JwtService.TOKEN_PREFIX + jwtUser)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.status").value(EventStatus.IN_PROGRESS.toString()));
    }

    @Test
    void whenCreateEventShouldReturnBadRequest() throws Exception {
        mockMvc.perform(post(PATH_SCHEDULER + "/{business-id}", 999)
                .header("Authorization", JwtService.TOKEN_PREFIX + jwtUser)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void whenConfirmEventShouldConfirmIt() throws Exception {
        mockMvc.perform(patch(PATH_SCHEDULER + "/confirm/{event-id}", eventId)
                .header("Authorization", JwtService.TOKEN_PREFIX + jwtBusiness))
                .andExpect(status().isOk());
    }

    @Test
    void whenCompleteEventShouldConfirmIt() throws Exception {
        mockMvc.perform(patch(PATH_SCHEDULER + "/complete/{event-id}", eventId)
                .header("Authorization", JwtService.TOKEN_PREFIX + jwtBusiness))
                .andExpect(status().isOk());
    }

    @Test
    void whenDeclineEventShouldConfirmIt() throws Exception {
        mockMvc.perform(patch(PATH_SCHEDULER + "/decline/{event-id}", eventId)
                .header("Authorization", JwtService.TOKEN_PREFIX + jwtBusiness))
                .andExpect(status().isOk());
    }
}
